package com.example.account.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.account.Model.Account;
import com.example.account.Model.Customer;

@Service
public class AccountService {
    public List<Account> createAccount() {
        ArrayList<Account> accountList = new ArrayList<>();
        Customer customer1 = new Customer(1, "tam", 20);
        Customer customer2 = new Customer(1, "2", 30);
        Customer customer3 = new Customer(1, "tam", 40);

        Account account1 = new Account(1, customer1, 2000.000);
        Account account2 = new Account(1, customer2, 4000.000);
        Account account3 = new Account(1, customer3, 5000.000);
        accountList.addAll(Arrays.asList(account1, account2, account3));
        return accountList;
    }
}
