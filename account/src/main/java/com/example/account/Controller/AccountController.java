package com.example.account.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.account.Model.Account;
import com.example.account.Service.AccountService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AccountController {
    @Autowired
    private AccountService service;

    @GetMapping("/accounts")
    public List<Account> getAccount() {
        return service.createAccount();

    }
}
